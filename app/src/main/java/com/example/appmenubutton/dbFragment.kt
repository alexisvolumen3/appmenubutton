
import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.example.appmenubutton.AlumnoLista
import com.example.appmenubutton.R
import com.example.appmenubutton.database.Alumno
import com.example.appmenubutton.database.dbAlumnos
import java.io.IOException
import kotlin.math.log
import com.bumptech.glide.Glide

class DbFragment : Fragment() {
    private lateinit var btnGuardar:Button
    private lateinit var btnBuscar:Button
    private lateinit var btnBorrar:Button
    private lateinit var btnLimpiar:Button
    private lateinit var txtMatricula:EditText
    private lateinit var txtNombre:EditText
    private lateinit var txtDomicilio:EditText
    private lateinit var txtEspecialidad:EditText
    private lateinit var txtUrlImagen:EditText
    private lateinit var imgAlumno:ImageView

    private lateinit var db: dbAlumnos
    private var imageUri: Uri? = null

    // Validaciaones externas para buscar
    private var valiBuscar = 0
    private var valiAct = 0



    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(
            R.layout.fragment_db, container,
            false
        )

        // Inicializando Comoponentes

        btnGuardar = view.findViewById(R.id.btnGuardar)
        btnBuscar = view.findViewById(R.id.btnBuscar)
        btnBorrar = view.findViewById(R.id.btnBorrar)
        btnLimpiar = view.findViewById(R.id.btnLimpiar)
        txtMatricula = view.findViewById(R.id.txtMatricula)
        txtNombre = view.findViewById(R.id.txtNombre)
        txtDomicilio = view.findViewById(R.id.txtDomicilio)
        txtEspecialidad = view.findViewById(R.id.txtEspecialidad)
        txtUrlImagen = view.findViewById(R.id.txtFoto)
        imgAlumno = view.findViewById(R.id.imgAlumno)


        arguments?.let{
            val alumnoLista = it.getSerializable("mialumno") as AlumnoLista
            txtNombre.setText(alumnoLista.nombre)
            txtDomicilio.setText(alumnoLista.domicilio)
            txtEspecialidad.setText(alumnoLista.especialidad)
            txtMatricula.setText(alumnoLista.matricula)
            txtUrlImagen.setText(alumnoLista.foto)
            Glide.with(this)
                .load(alumnoLista.foto)
                .placeholder(R.drawable.img) // Placeholder en caso de que no se cargue la imagen
                .into(imgAlumno)

            //imgAlumno.setImageURI(Uri.parse((alumnoLista.foto)))
        }

        imgAlumno.setOnClickListener{
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(intent, 1000)

        }

        // Creando Botón Guardar
        btnGuardar.setOnClickListener{
            if(valiEmpty()){
                Toast.makeText(requireContext(),"Porfavor ingrese toda la información solicitada",Toast.LENGTH_SHORT).show()
            }
            else{
                if(valiBuscar == 0 || valiAct == 0){
                    db = dbAlumnos(requireContext())
                    db.openDataBase()

                    val isExistAlumno = db.getAlumno(txtMatricula.text.toString())
                    if(isExistAlumno.matricula.equals("")){
                        var alumnos: Alumno
                        alumnos = Alumno()

                        alumnos.nombre = txtNombre.text.toString()
                        alumnos.matricula = txtMatricula.text.toString()
                        alumnos.domicilio = txtDomicilio.text.toString()
                        alumnos.especialidad = txtEspecialidad.text.toString()
                        alumnos.foto = imageUri?.toString() ?:"Pendiente"


                        var id:Long = db.InsertarAlumno(alumnos)
                        Toast.makeText(requireContext(), "Se agrego con exito", Toast.LENGTH_SHORT).show()
                        db.close()
                        limpiar()
                        valiAct = 1
                    }
                    else{
                        var alumnos: Alumno
                        alumnos = Alumno()

                        alumnos.nombre = txtNombre.text.toString()
                        alumnos.matricula = txtMatricula.text.toString()
                        alumnos.domicilio = txtDomicilio.text.toString()
                        alumnos.especialidad = txtEspecialidad.text.toString()
                        alumnos.foto = imageUri?.toString() ?:"Pendiente"



                        db.ActualizarAlumno(alumnos)
                        Toast.makeText(requireContext(), "Actualizado con Exito", Toast.LENGTH_SHORT).show()
                        db.close()
                        limpiar()
                        valiBuscar = 0
                    }
                    //Log.d("AcercaFragment", "Alumnos retrieved: ${isExistAlumno}")



                }
                else{
                    var alumnos: Alumno
                    alumnos = Alumno()

                    alumnos.nombre = txtNombre.text.toString()
                    alumnos.matricula = txtMatricula.text.toString()
                    alumnos.domicilio = txtDomicilio.text.toString()
                    alumnos.especialidad = txtEspecialidad.text.toString()
                    alumnos.foto = imageUri?.toString() ?:"Pendiente"

                    db = dbAlumnos(requireContext())
                    db.openDataBase()

                    db.ActualizarAlumno(alumnos)
                    Toast.makeText(requireContext(), "Actualizado con Exito", Toast.LENGTH_SHORT).show()
                    db.close()
                    limpiar()
                    valiBuscar = 0
                }


            }
        }

        btnBuscar.setOnClickListener{
            if(txtMatricula.text.toString().contentEquals("")){
                Toast.makeText(requireContext(), "Porfavor ingrese la matricula para buscar", Toast.LENGTH_SHORT).show()
            }
            else{
                db = dbAlumnos(requireContext())
                db.openDataBase()
                var alumno:Alumno = Alumno()
                alumno = db.getAlumno(txtMatricula.text.toString())
                if(alumno.id!=0){
                    txtNombre.setText(alumno.nombre)
                    txtDomicilio.setText(alumno.domicilio)
                    txtEspecialidad.setText(alumno.especialidad)
                    txtUrlImagen.setText(alumno.foto)
                    Glide.with(this)
                        .load(alumno.foto)
                        .placeholder(R.drawable.img) // Placeholder en caso de que no se cargue la imagen
                        .into(imgAlumno)
                    valiBuscar = 1
                }
                else{
                    Toast.makeText(requireContext(), "La matricula ingresada no existe", Toast.LENGTH_SHORT).show()
                }
            }
        }

        btnBorrar.setOnClickListener{
            if(txtMatricula.text.toString().contentEquals("")){
                Toast.makeText(requireContext(), "Porfavor ingrese la matricula para buscar", Toast.LENGTH_SHORT).show()
            }
            else{
                val builder = AlertDialog.Builder(requireContext())
                builder.setTitle("Confirmación")
                builder.setMessage("¿Estás seguro de que deseas borrar este Alumno?")

                // Configurar el botón "Sí"
                builder.setPositiveButton("Sí") { dialog, which ->
                    var alumno: Alumno
                    alumno = Alumno()

                    db = dbAlumnos(requireContext())
                    db.openDataBase()
                    alumno = db.getAlumno(txtMatricula.text.toString())
                    if(alumno.id!=0){
                        db.BorrarAlumno(txtMatricula.text.toString())
                        Toast.makeText(requireContext(), "Alumno eliminado con exito", Toast.LENGTH_SHORT).show()
                        limpiar()
                    }
                    else{
                        Toast.makeText(requireContext(), "La matricula ingresada no existe", Toast.LENGTH_SHORT).show()
                    }
                }

                // Configurar el botón "No"
                builder.setNegativeButton("No") { dialog, which ->
                }

                // Crear y mostrar el diálogo
                val dialog: AlertDialog = builder.create()
                dialog.show()


            }

        }

        btnLimpiar.setOnClickListener{
            limpiar()
        }


        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1000 && resultCode == Activity.RESULT_OK) {
            imageUri = data?.data
            try {
                imgAlumno.setImageURI(imageUri)
                txtUrlImagen.text = Editable.Factory.getInstance().newEditable(imageUri.toString())



            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }

    fun valiEmpty():Boolean{
        if(txtNombre.text.toString().contentEquals("")||
            txtDomicilio.text.toString().contentEquals("")||
            txtMatricula.text.toString().contentEquals("")||
            txtEspecialidad.text.toString().contentEquals("")){
            return true

        }
        else{
            return false
        }
    }

    fun limpiar(){
        txtMatricula.text.clear()
        txtNombre.text.clear()
        txtDomicilio.text.clear()
        txtEspecialidad.text.clear()
        txtUrlImagen.text.clear()
        imgAlumno.setImageResource(R.drawable.img)
    }

}