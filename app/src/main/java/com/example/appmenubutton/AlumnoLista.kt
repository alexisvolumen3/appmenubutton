package com.example.appmenubutton

import java.io.Serializable

class AlumnoLista (
    val id: Int,
    var matricula:String="",
    var nombre:String="",
    var domicilio:String="",
    var especialidad:String="",
    var foto:String=""
): Serializable
